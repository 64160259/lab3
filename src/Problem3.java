import java.util.Scanner;

public class Problem3 {
    public static void main(String[] args) {
        int num1;
        int num2;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input first number: ");
        num1 = sc.nextInt();
        System.out.print("Please input second number: ");
        num2 = sc.nextInt();
        if (num1<=num2) {
            int i=num1;
            while(i<=num2) {
                System.out.print(i + " ");
                i++;
            }
        } else {
                System.out.print("Error");
        }
        sc.close();
        
    }
    
}
