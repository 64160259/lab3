import java.util.Scanner;

public class Problem9While {
    public static void main(String[] args) {
        int n;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input n: ");
        n=sc.nextInt();
        if(n<4) {
            int i=1;
            while(i<4) {
                int j=1;
                while(j<4) {
                    System.out.print(j);
                    j++;
                }
                System.out.println();
                i++;
            }
        }
    }
    
}
