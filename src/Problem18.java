import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(true) {
            System.out.print("Please select star type[1-4,5 is Exit]: ");
            int type = sc.nextInt();
            if (type==1) {
                System.out.print("Please input number: ");
                int num = sc.nextInt();
                for (int i=0; i<num; i++) {
                    for (int j=0; j<=i; j++) {
                        System.out.print("*");
                    }
                    System.out.println();
                }
        } else if(type==2) {
            System.out.print("Please input number: ");
                int num = sc.nextInt();
                for (int i = num; i > 0; i--) {
                    for (int j = 0; j < i; j++) {
                        System.out.print("*");
                    }
                    System.out.println();
                }
        } else if(type==3) {
            System.out.print("Please input number: ");
                int num = sc.nextInt();
                for (int i = 0; i < num; i++) {
                    for (int j = 0; j < num; j++) {
                        if (j >= i) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }
        } else if(type==4) {
            System.out.print("Please input number: ");
                int num = sc.nextInt();
                for (int i = num; i > 0; i--) {
                    for (int j = 0; j <= num; j++) {
                        if (j >= i) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }
        } else if(type==5) {
            System.out.println("Bye bye!!!");
                break;
        } else {
            System.out.println("Eror:Please input number between 1-5");
        }
    }
    sc.close();

   }    
}
